<?

include 'simple_html_dom.php';

$postData = $_POST;

$curlData = getCurlData($postData);

$points = parseLoyaltyPoints($curlData);
//header('Content-Type: application/json');
echo json_encode(array('points'=>$points));
exit;


function getCurlData($postData){
$url = "http://www.emirates.com/account/english/miles-calculator/miles-calculator.aspx?org=BOM&dest=DEL&trvc=0&h=7b1dc440b5eecbda143bd8e7b9ef53a27e364b";
$ckfile = tempnam("/tmp", "CURLCOOKIE");
$useragent = 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.2 (KHTML, like Gecko) Chrome/5.0.342.3 Safari/533.2';

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_COOKIEJAR, $ckfile);
curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_REFERER, $url);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
#curl_setopt($ch, CURLOPT_STDERR, $f);
curl_setopt($ch, CURLOPT_USERAGENT, $useragent);

// Collecting all POST fields
$postfields = array();
$postfields['__EVENTTARGET'] = "";
$postfields['__EVENTARGUMENT'] = "";
$postfields['__VIEWSTATEENCRYPTED'] = "";
$postfields['__VIEWSTATE'] = 'fnxji5+q7FlR61X/SfJbLjAegets/xN8Re7xPLMKk3V2jMdx1Yu3m1CspxMOOTL+2gtTppKeVXhbu4StZfBG8pK4Xl9wNGCGkU14nEBPuAYhTFEslKXR0HRauwCCpIHmTB0Hm5C6uRnrPGyzBygTht/TIuZKrChnxKNZquNYP6whBsqvBjVXvJQiqO3KdXhzCyTV7COmrIUNwOHzl9yMfx2DdYM00iE8Iepu3V2aQ6Feq/WJz8A7sBtv9UEPPWooT1i6dBWeZ4eibnzmujj0jFYgEgfNusgYf/TwW+zzP+xlhBSxZuO2nIheOBdirkgDc9TqVUdI0RHh1b/zp79bfrUGoSwdlr/pkr4OJ9mwNJBNyTE2oUoU4S5KOQA3JvGsTYxOKffhJF0wflVlyNk93B6dCd0bwArJRyx/9xAZu+mDgVE6r1/9N4rpxqPSXzWuEN2s52qIa+icN/hkJhQlgFmrvU8PIiba+TRly/ulP4QCCGj2B3jBLi32PzXTU73tZtkGHTis9bnJnj4U202EzsiEADdalZLjETmebCJfGwL4eIbK01mEWI2LqVRavZczLVFrcJY2B6t8Z2/eH9uzDzQfd3PvPDXMdhDYUh0n/KyCawetr1HqTwIK7vYNwpzKpB47EE57S7G7DZkcfUDqQmCaVARGdFcsv21Eb2xlE5Xumuzhz6QL9FaAWnnEHjahjiwRGwNtqz1rEwzl0Jhx/0/9VZcaALM2uox1b/GehoBIXs+jPDqPxILrXj2OCEcNhIEbzxJVSF7PxhovlIQNhLpDCic73qM6M3gQniR02XKwp18fMOhRV/T/V9PLfQrRxOCEXlgC30tMDwGX6XTPARIyBLZP360jVRzwx7gIfM8lzFiJTRUjGetI5ABlZeqJrkibFqWVpbcRf/uiH68OeJTMO2kuYJHq2RwxMhVMSz1cVqNVUiUPhgJNFFFRgSlx+bN8zk0TSiZmz91l79lLA8tApjCR1KWdX7OmJfEIA100vrGu';
$postfields['__EVENTVALIDATION'] = '';
$postfields['txtHeaderSearch'] = 'Search';

$postfields['siteSelectorID'] = '0';
$postfields['ddlDepartureAirport'] = mysql_real_escape_string($postData['deptAirport']);
$postfields['ddlDepartureAirport-suggest'] = mysql_real_escape_string($postData['deptAirportLoc']);
$postfields['ddlArrivalAirport'] = mysql_real_escape_string($postData['arrivalAirport']);
$postfields['ddlArrivalAirport-suggest'] = mysql_real_escape_string($postData['arrivalAirportLoc']);
$postfields['ctl00$MainContent$ctl02$Cabin'] = 'rd' . $postData['class'];
$postfields['ctl00$MainContent$ctl02$btnSubmit'] = 'Search';
$postfields['hfTierOrder'] = '1';
$postfields['ctl00$MainContent$ctl03$ctl00$hdnCheck'] = '1';
$postfields['Itinerary'] = 'rbReturn';
$postfields['seldcity1'] = '';
$postfields['seldcity1-suggest'] = 'Departure Airport';
$postfields['selddate1'] = '24 Jun 14';
$postfields['selcabinclass'] = '0';
$postfields['selacity1'] = '';
$postfields['selacity1-suggest'] = 'Arrival Airport';
$postfields['seladate1'] = '';
$postfields['selcabinclass1'] = '0';
$postfields['seladults'] = '1';
$postfields['selchildren'] = '0';
$postfields['selinfants'] = '0';
$postfields['redeemforValue'] = '';
$postfields['j'] = 't';
$postfields['multiCity'] = '';
$postfields['chkReturn'] = 'on';
$postfields['redeemChecked'] = '';
$postfields['redeemfor'] = '';
$postfields['flex'] = '0';
$postfields['interline'] = '0';
$postfields['signon'] = '';
$postfields['departDate'] = '24062014';
$postfields['EnableInterlinePriceOptions'] = '0';
$postfields['DefaultInterlineResultsBy'] = '1';
$postfields['DefaultResultsBy'] = '2';
$postfields['currentPanelOpen'] = '';

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

$ret = curl_exec($ch);
curl_close($ch);

return $ret;
}

function parseLoyaltyPoints($content){
$html = new simple_html_dom();
$html = str_get_html($content);
for($i=0; $i<=3; $i++){
$return_saverfare_blue[$i] = $html->find('div#bluereturnEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$oneway_saverfare_blue[$i] = $html->find('div#blueonewayEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$return_saverfare_silver[$i] = $html->find('div#silverreturnEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$oneway_saverfare_silver[$i] = $html->find('div#silveronewayEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$return_saverfare_gold[$i] = $html->find('div#goldreturnEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$oneway_saverfare_gold[$i] = $html->find('div#goldonewayEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$return_saverfare_platinum[$i] = $html->find('div#platinumreturnEarn', 0)->find('td', $i)->plaintext;
}

for($i=0; $i<=3; $i++){
$oneway_saverfare_platinum[$i] = $html->find('div#platinumonewayEarn', 0)->find('td', $i)->plaintext;
}

$points = array('Blue_Return' => array('Saver Skyware Miles' => $return_saverfare_blue[0], 'Saver Tier Miles' => $return_saverfare_blue[2], 'Flex Skyware Miles' => $return_saverfare_blue[1], 'Flex Tier Miles' => $return_saverfare_blue[3]),
				'Blue_Oneway' => array('Saver Skyware Miles' => $oneway_saverfare_blue[0], 'Saver Tier Miles' => $oneway_saverfare_blue[2], 'Flex Skyware Miles' => $oneway_saverfare_blue[1], 'Flex Tier Miles' => $oneway_saverfare_blue[3]),
				'Silver_Return' => array('Saver Skyware Miles' => $return_saverfare_silver[0], 'Saver Tier Miles' => $return_saverfare_silver[2], 'Flex Skyware Miles' => $return_saverfare_silver[1], 'Flex Tier Miles' => $return_saverfare_silver[3]),
				'Silver_Oneway' => array('Saver Skyware Miles' => $oneway_saverfare_silver[0], 'Saver Tier Miles' => $oneway_saverfare_silver[2], 'Flex Skyware Miles' => $oneway_saverfare_silver[1], 'Flex Tier Miles' => $oneway_saverfare_silver[3]),
				'Gold_Return' => array('Saver Skyware Miles' => $return_saverfare_gold[0], 'Saver Tier Miles' => $return_saverfare_gold[2], 'Flex Skyware Miles' => $return_saverfare_gold[1], 'Flex Tier Miles' => $return_saverfare_gold[3]),
				'Gold_Oneway' => array('Saver Skyware Miles' => $oneway_saverfare_gold[0], 'Saver Tier Miles' => $oneway_saverfare_gold[2], 'Flex Skyware Miles' => $oneway_saverfare_gold[1], 'Flex Tier Miles' => $oneway_saverfare_gold[3]),
				'Platinum_Return' => array('Saver Skyware Miles' => $return_saverfare_platinum[0], 'Saver Tier Miles' => $return_saverfare_platinum[2], 'Flex Skyware Miles' => $return_saverfare_platinum[1], 'Flex Tier Miles' => $return_saverfare_platinum[3]),
				'Platinum_Oneway' => array('Saver Skyware Miles' => $oneway_saverfare_platinum[0], 'Saver Tier Miles' => $oneway_saverfare_platinum[2], 'Flex Skyware Miles' => $oneway_saverfare_platinum[1], 'Flex Tier Miles' => $oneway_saverfare_platinum[3]));

return $points;
}

